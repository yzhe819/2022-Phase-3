using back_end.Data;
using back_end.Models;
using Microsoft.EntityFrameworkCore;

namespace back_end_test;

public class Tests
{
    private DBWebAPIRepo _dbWebApiRepo;

    [SetUp]
    public void Setup()
    {
        var options = new DbContextOptionsBuilder<WebAPIDBContext>()
            .UseInMemoryDatabase("MemoryTesting").Options;

        var context = new WebAPIDBContext(options);
        _dbWebApiRepo = new DBWebAPIRepo(context);
    }

    [Test]
    [Order(1)]
    public void TestGetPlayers()
    {
        var list = _dbWebApiRepo.GetTopPlayers();
        Assert.AreEqual(list.Count(), 0);
    }

    [Test]
    [Order(2)]
    public void TestAddPlayers()
    {
        _dbWebApiRepo.AddPlayer(new Player
        {
            username = "test1",
            score = 80
        });
        _dbWebApiRepo.AddPlayer(new Player
        {
            username = "test2",
            score = 90
        });
        var list = _dbWebApiRepo.GetTopPlayers();
        Assert.AreEqual(list.Count(), 2);
    }

    [Test]
    [Order(3)]
    public void TestOrder()
    {
        // higher mark first
        var list = _dbWebApiRepo.GetTopPlayers();
        var first = list.First();
        var second = list.ElementAt(1);
        Assert.Greater(first.score, second.score);
    }

    [Test]
    [Order(4)]
    public void TestGetSinglePlayer()
    {
        var list = _dbWebApiRepo.GetTopPlayers();
        var id = list.First().id;
        var res = _dbWebApiRepo.GetPlayerById(id);
        Assert.AreEqual(list.First().username, res.username);
        Assert.AreEqual(list.First().score, res.score);
        Assert.AreEqual(list.First().id, res.id);
    }

    [Test]
    [Order(5)]
    public void TestDelete()
    {
        var list = _dbWebApiRepo.GetTopPlayers();
        Assert.AreEqual(list.Count(), 2);
        var id = list.First().id;
        _dbWebApiRepo.DeletePlayerById(id);
        list = _dbWebApiRepo.GetTopPlayers();
        Assert.AreEqual(list.Count(), 1);
    }
}