### URL

frontend: http://2022-phase-3-sigma.vercel.app

backend: https://msa-yzhe819.azurewebsites.net/WeatherForecast



## FRONTEND: Advanced Features List

- [x] UI Scalability with window size
  - using tailwind CSS to build the responsive web application
- [x] Mobile first development (using media query breakpoints, etc)
  - the same as above
- [x] API connection to your own API that is cloud hosted
  - use Next.js framework to build, which provide a build-in API service
- [x] At least one fluid animation
  - the confetti animation when the player finished his game
- [x] Redux state management
  - use redux to do the state management
- [x] Demonstration of complex FE logic
  - full logic of the quiz game



## BACKEND: Advanced Features List

- [x] Onion structure - clear separation of DB access layer and API layer
- [x] Usage of EF Core
- [x] Comprehensive unit testing
- [x] Deployment using a CI/CD pipeline to the cloud
- [x] Usage of Fluent Validation / Fluent Assertions
