using back_end.Dto;
using FluentValidation;

namespace back_end.Validator;

public class PlayerValidator : AbstractValidator<PlayerDto>
{
    public PlayerValidator()
    {
        RuleFor(player => player.name).NotNull();
        // the score should be in the range: 0 ~ 100
        RuleFor(player => player.score).GreaterThanOrEqualTo(0);
        RuleFor(player => player.score).LessThanOrEqualTo(100);
    }
}