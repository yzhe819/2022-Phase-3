namespace back_end.Dto;

public class PlayerDto
{
    public string name { get; set; }
    public int score { get; set; }
}