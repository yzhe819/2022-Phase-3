using back_end.Data;
using back_end.Dto;
using back_end.Models;
using back_end.Validator;
using FluentValidation.Results;
using Microsoft.AspNetCore.Mvc;

namespace back_end.Controllers;

[Route("webapi")]
[ApiController]
public class PlayerController : Controller
{
    private readonly IWebAPIRepo _repo;

    public PlayerController(IWebAPIRepo repo)
    {
        _repo = repo;
    }

    [HttpGet("GetPlayers")]
    public ActionResult<IEnumerable<Player>> GetCustomers()
    {
        var players = _repo.GetTopPlayers();
        return Ok(players);
    }

    [HttpPost("AddPlayer")]
    public ActionResult<PlayerDto> AddPlayer(PlayerDto input)
    {
        PlayerValidator validator = new PlayerValidator();
        ValidationResult result = validator.Validate(input);

        if (!result.IsValid)
        {
            // invalid request data
            return BadRequest();
        }
        
        var p = new Player
        {
            username = input.name,
            score = input.score
        };
        var addedPlayer = _repo.AddPlayer(p);
        return Ok(addedPlayer);
    }

    [HttpGet("GetPlayer/{id}")]
    public ActionResult<IEnumerable<Player>> GetPlayerByID(int id)
    {
        var player = _repo.GetPlayerById(id);
        if (player == null) return NotFound();
        return Ok(player);
    }


    [HttpDelete("DeletePlayer/{id}")]
    public ActionResult<IEnumerable<Player>> DeletePlayerByID(int id)
    {
        _repo.DeletePlayerById(id);
        return Ok();
    }
}