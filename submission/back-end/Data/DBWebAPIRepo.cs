using back_end.Models;

namespace back_end.Data;

public class DBWebAPIRepo : IWebAPIRepo
{
    private readonly WebAPIDBContext _dbContext;

    public DBWebAPIRepo(WebAPIDBContext dbContext)
    {
        _dbContext = dbContext;
    }

    public IEnumerable<Player> GetTopPlayers()
    {
        // only get the top 10 players, which has the highest score
        IEnumerable<Player> players = _dbContext.Players.OrderByDescending(player => player.score).Take(10).ToList();
        return players;
    }

    public Player AddPlayer(Player player)
    {
        var e = _dbContext.Players.Add(player);
        var p = e.Entity;
        _dbContext.SaveChanges();
        return p;
    }

    public Player GetPlayerById(int id)
    {
        var player = _dbContext.Players.FirstOrDefault(e => e.id == id);
        return player;
    }

    public void DeletePlayerById(int id)
    {
        var player = GetPlayerById(id);
        _dbContext.Players.Remove(player);
        _dbContext.SaveChanges();
    }
}