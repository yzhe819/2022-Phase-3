using back_end.Models;
using Microsoft.EntityFrameworkCore;

namespace back_end.Data;

public class WebAPIDBContext : DbContext
{
    public WebAPIDBContext(DbContextOptions<WebAPIDBContext> options) : base(options)
    {
    }

    public DbSet<Player> Players { get; set; }
}