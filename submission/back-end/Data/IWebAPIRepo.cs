using back_end.Models;

namespace back_end.Data;

public interface IWebAPIRepo
{
    IEnumerable<Player> GetTopPlayers();
    Player AddPlayer(Player player);
    Player GetPlayerById(int id);
    void DeletePlayerById(int id);
}