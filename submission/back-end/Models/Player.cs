using System.ComponentModel.DataAnnotations;

namespace back_end.Models;

public class Player
{
    [Key] public int id { get; set; }

    [Required] public string username { get; set; }

    [Required] public int score { get; set; }
}