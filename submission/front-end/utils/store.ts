import { createSlice, configureStore } from "@reduxjs/toolkit";
import { Question } from "../pages/api/question";

export interface GlobalState {
  username?: string;
  score: number;
  questionIndex: number;
  questionList: Question[];
}

const initialState: GlobalState = {
  username: undefined,
  score: 0,
  questionIndex: 0,
  questionList: [],
};

const globalSlice = createSlice({
  name: "global",
  initialState,
  reducers: {
    setUsername(state, action) {
      state.username = action.payload;
    },
    AddScore(state, action) {
      state.score += action.payload;
    },
    resetGame(state, _action) {
      state.score = 0;
      state.questionIndex = 0;
      state.questionList = [];
    },
    setQuestionList(state, action) {
      state.questionIndex = 0;
      state.questionList = action.payload;
    },
    nextQuestion(state, _action) {
      state.questionIndex++;
    },
  },
});

const store = configureStore({
  reducer: globalSlice.reducer,
});

store.subscribe(() => console.log(store.getState()));

export default store;
