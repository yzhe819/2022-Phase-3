// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from "next";
import shuffle from "../../utils/shuffle";

type Data = {
  question: string;
  correct_answer: string;
  incorrect_answers: string[];
};

export type Question = {
  question: string;
  correct_answer: string;
  answers: string[];
};

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse<Question[]>
) {
  const questionList = await fetch(
    "https://opentdb.com/api.php?amount=10"
  ).then((res) => res.json());
  const result: any[] = [];
  questionList.results.forEach((element: Data) => {
    const tem = {
      question: element.question,
      correct_answer: element.correct_answer,
      answers: element.incorrect_answers,
    };
    tem.answers.push(tem.correct_answer);
    // shuffle the answer
    shuffle(tem.answers);
    result.push(tem);
  });
  res.status(200).json(result);
}
