import { useRouter } from "next/router";
import { useEffect } from "react";
import { useStore } from "react-redux";

export default function ResultPage() {
  const router = useRouter();
  const store = useStore();
  const { questionList, questionIndex } = store.getState() as any;
  const questionDetails = questionList[questionIndex];

  useEffect(() => {
    if (questionList.length == 0) {
      router.push("/");
    }
  }, []);

  return (
    questionDetails && (
      <div className="flex flex-col items-center justify-center h-screen max-w-sm gap-4 m-auto">
        <h1 className="w-full text-3xl font-bold">
          {questionDetails.question}
        </h1>
        <p className="w-full text-3xl font-bold text-gray-500 capitalize">
          correct answer: {questionDetails.correct_answer}
        </p>
        <button
          className="w-full px-4 py-2 mt-2 font-bold text-white bg-purple-500 rounded shadow hover:bg-purple-400"
          type="button"
          onClick={() => {
            if (questionIndex == questionList.length - 1) {
              router.push("/complete");
            }
            store.dispatch({
              type: "global/nextQuestion",
            });
            router.push("/question");
          }}
        >
          Continue
        </button>
      </div>
    )
  );
}
