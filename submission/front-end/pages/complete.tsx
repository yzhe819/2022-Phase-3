import { useEffect, useState } from "react";
import useWindowSize from "react-use/lib/useWindowSize";
import Confetti from "react-confetti";
import { useRouter } from "next/router";
import { useStore } from "react-redux";

export default function CompletePage() {
  // setting up the confetti
  const { width, height } = useWindowSize();
  const [isLoading, setLoaded] = useState(false);

  const router = useRouter();
  const store = useStore();
  const { username, score } = store.getState() as any;

  useEffect(() => {
    setLoaded(true);
  }, []);

  return (
    <>
      {isLoading && <Confetti width={width} height={height} recycle={false} />}
      <div className="flex flex-col items-center justify-center h-screen max-w-sm gap-4 p-4 m-auto">
        <h1 className="w-full text-3xl font-bold">
          Congratulations, {username}! 🎉
        </h1>
        <h1 className="w-full text-3xl font-bold">Your score is {score}/100</h1>
        <button
          className="w-full px-4 py-2 mt-2 font-bold text-white bg-purple-500 rounded shadow hover:bg-purple-400"
          type="button"
          onClick={() => {
            store.dispatch({
              type: "global/resetGame",
            });
            router.push("/start");
          }}
        >
          Try another game
        </button>
      </div>
    </>
  );
}
