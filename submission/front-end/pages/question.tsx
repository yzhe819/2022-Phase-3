import {
  BsFillSquareFill,
  BsFillCircleFill,
  BsFillTriangleFill,
} from "react-icons/bs";
import { useRouter } from "next/router";
import { useEffect } from "react";
import { useStore } from "react-redux";

const color = ["bg-red-400", "bg-blue-300", "bg-amber-300", "bg-lime-300"];
const icons = [
  <BsFillTriangleFill key={0} className="text-4xl text-white" />,
  <BsFillSquareFill key={1} className="text-4xl text-white" />,
  <BsFillCircleFill key={2} className="text-4xl text-white" />,
  <BsFillSquareFill key={3} className="text-4xl text-white" />,
];

export default function QuestionPage() {
  const router = useRouter();
  const store = useStore();
  const { questionList, questionIndex } = store.getState() as any;
  const questionDetails = questionList[questionIndex];

  useEffect(() => {
    if (questionList.length == 0) {
      router.push("/");
    }
  }, []);

  return (
    questionDetails && (
      <div className="flex flex-col items-center justify-center h-screen gap-8 p-8">
        <h1 className="text-3xl font-bold break-all">
          {questionDetails.question}
        </h1>
        <div className="grid flex-1 w-full grid-cols-1 grid-rows-4 gap-4 md:grid-cols-2 md:grid-rows-2">
          {questionDetails.answers.map((answer: string, index: number) => {
            // style of card
            const style =
              "flex flex-row items-center w-full h-full gap-4 px-8 rounded cursor-pointer " +
              color[index];
            return (
              <div
                key={index}
                className={style}
                onClick={() => {
                  if (answer == questionDetails.correct_answer) {
                    store.dispatch({
                      type: "global/AddScore",
                      payload: 10,
                    });
                  }
                  router.push("/result");
                }}
              >
                {icons[index]}
                <p className="text-2xl font-bold text-white">{answer}</p>
              </div>
            );
          })}
        </div>
      </div>
    )
  );
}
