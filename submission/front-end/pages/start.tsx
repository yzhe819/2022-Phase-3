import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import { useStore } from "react-redux";

export default function StartPage() {
  const router = useRouter();
  const store = useStore();
  const { username } = store.getState() as any;
  const [isReady, setReadyState] = useState(false);

  useEffect(() => {
    fetch("/api/question")
      .then((res) => res.json())
      .then((data) => {
        store.dispatch({
          type: "global/setQuestionList",
          payload: data,
        });
        setReadyState(true);
      });
  }, []);

  return (
    <div className="flex flex-col items-center justify-center h-screen max-w-sm gap-4 p-4 m-auto">
      <h1 className="w-full text-3xl font-bold">Hey {username}👋</h1>
      <p className="font-bold text-gray-500">
        When you&#39;re ready, click the button below to start the game! 💥
      </p>
      {isReady ? (
        <button
          className="w-full px-4 py-2 mt-2 font-bold text-white bg-purple-500 rounded shadow hover:bg-purple-400"
          type="button"
          onClick={() => {
            router.push("/question");
          }}
        >
          Next
        </button>
      ) : (
        <button
          className="w-full px-4 py-2 mt-2 font-bold text-white bg-purple-300 rounded shadow"
          type="button"
          disabled
        >
          Loading
        </button>
      )}
    </div>
  );
}
