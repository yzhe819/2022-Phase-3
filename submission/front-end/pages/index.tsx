import { useRouter } from "next/router";
import { useStore } from "react-redux";

export default function LoginPage() {
  const router = useRouter();
  const store = useStore();
  return (
    <div className="flex flex-col items-center justify-center h-screen max-w-sm gap-4 p-4 m-auto">
      <h1 className="text-3xl font-bold capitalize">
        Input your username. <br /> Join the quiz game here! 💥
      </h1>
      <input
        className="w-full px-3 py-2 font-bold leading-tight text-gray-600 border rounded focus:outline-none"
        id="username"
        type="text"
        placeholder="Username"
        onChange={(e) => {
          store.dispatch({
            type: "global/setUsername",
            payload: e.target.value,
          });
        }}
      />
      <button
        className="w-full px-4 py-2 mt-2 font-bold text-white bg-purple-500 rounded shadow hover:bg-purple-400"
        type="button"
        onClick={() => {
          router.push("/start");
        }}
      >
        Continue
      </button>
    </div>
  );
}
